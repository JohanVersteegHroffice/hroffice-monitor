import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { Http } from '@angular/http';


@Injectable()
export class EtlService {

    constructor(private http: Http) {

    }

    getDasboard1(): Promise<any> {
        return this.http.get('http://hrofficeetl.azurewebsites.net/api/Dashboard')
            .toPromise()
            .then(response => {
                return response.json();
            })
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}
