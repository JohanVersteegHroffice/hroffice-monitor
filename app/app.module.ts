import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { Monitor1Component } from './monitors/monitor1.component';
import { EtlService } from './services/etl.service';

@NgModule({
  imports: [
    BrowserModule,
    HttpModule
  ],
  declarations: [AppComponent, Monitor1Component],
  providers: [EtlService],
  bootstrap: [AppComponent]
})
export class AppModule { }
