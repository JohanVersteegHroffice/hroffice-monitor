import { Component } from '@angular/core';
import { EtlService } from './../services/etl.service';
@Component({
    templateUrl: 'views/monitor1.html',
    selector: "monitor1"
})
export class Monitor1Component {

    public data: any;

    constructor(private etlService: EtlService) {
        etlService.getDasboard1().then(data => {
            this.data = data;
            console.log(data);
        })
    }

} 